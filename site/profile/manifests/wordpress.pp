class profile::wordpress {
    include apache
    include wordpress
    include mysql::server
    include apache::mod::php
    package { 'php-mysqlnd':
      ensure => installed,
      notify => Service['httpd'],
    }
    file { ['/etc/puppetlabs/facter', '/etc/puppetlabs/facter/facts.d']:
      ensure => directory
    }

    file { '/etc/puppetlabs/facter/facts.d/my_application.txt':
      ensure => file,
      source => 'puppet:///modules/profile/application_fact.txt',
    }

    file { '/root/blah':
      ensure => directory,
      recurse => true,
      source => 'puppet:///modules/profile/blah'
    }
}