class profile::base::packages (
  Array $package_list = ['tree'],
) {
  package { $package_list:
    ensure => installed,
  }
}