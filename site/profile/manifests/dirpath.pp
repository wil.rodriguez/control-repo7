define profile::dirpath (
  String $path = $name,
  String $owner = 'root',
  String $group = 'root',
) {
  exec { "mkdir ${path}":
    command  => "mkdir -p ${path}",
    creates  => $path,
    provider => shell,
  }

  #TODO Fix this
  exec { "chown ${path}":
    command  => "chown -R ${owner}:${group} ${path}",
    onlyif   => "find ${path} \\! -user ${owner} -o \\( \\! -group ${group} \\)",
    provider => shell,
  }
}